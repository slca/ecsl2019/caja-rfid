/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

//Angular resolution
fn = 64;

//Acrylic Backing
a_w = 5;

//Screw hole diameter
screw_d = 2.5;

//PCB width
pcb_w = 2;

//Header z
h_z = 10;

//Spacing
spacing = 1;

//Main Board
m_x = 32;
m_y = 58.5;
m_z = pcb_w;

//RFID Board
rf_x = 60;
rf_y = 40;
rf_z = pcb_w;

//Radio Board
rw_x = 16;
rw_y = 30;
rw_z = pcb_w;

//Battery Holder
bat_x = 32;
bat_y = 58;
bat_z = 28; 

//PCB layout
pcb_x = rf_x + m_x/2; 
pcb_y = rf_y + 2*spacing  + bat_y;
pcb_z = bat_z;

module batt_holder(x,y,z){
    cube(size=[x+spacing, y+spacing, z]);

    translate([x/2, 0,z/2])
    rotate(a=[90, 0, 0])
    cylinder(r=bat_z/3, h=2*spacing, $fn=fn);
    }
module main_pcb(x, y, z){
    cube(size=[x+spacing, y+spacing, z]);
    }
module rfid_pcb(x, y, z){
    cube(size=[x+spacing, y+spacing, z]);
    }
 module radio_pcb(x, y, z){
    cube(size=[x+spacing, y+spacing, z]);
    }
module case(x, y, z){
    difference(){
        minkowski(){
            cube(size=[x+2*spacing, y+2*spacing, z+spacing+a_w]);  // box
            sphere(r=2*spacing, $fn=fn);
            }
            translate([0, 0, z+3*spacing])
            cube(size=[x+spacing, y+spacing, a_w]);
        }
    }
module pcb_stack(){
    rfid_pcb(rf_x, rf_y, rf_z+50);
    translate([pcb_x-m_x, 0, pcb_w+h_z])    
        main_pcb(m_x, m_y, m_z+50);
    translate([rf_x-rw_x/2, rf_y+10,  2*pcb_w+2*h_z])
        radio_pcb(rw_x, rw_y, rw_z+50);
    }
//
difference(){
    difference(){
        case(pcb_x+2*spacing, pcb_y+2*spacing, pcb_z+spacing);
        translate([spacing, spacing, 0])
        group(){
            pcb_stack();
            translate([0, rf_y+2*spacing, 0])
                batt_holder(bat_x, bat_y, bat_z+h_z);
            }
        }
        translate([(pcb_x+4*spacing)/2, (rf_y+spacing+bat_y+4*spacing)/2, 2*spacing])
        cylinder(d=screw_d, h=pcb_z+10, $fn=6); // Screw hole
}

//batt_holder(bat_x, bat_y, bat_z+h_z);
//case(pcb_x+2*spacing, rf_y+bat_y+2*spacing, pcb_z+spacing);
//radio_pcb(rw_x, rw_y, rw_z+50);
//pcb_stack();
